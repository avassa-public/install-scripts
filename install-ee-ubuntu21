#!/bin/bash -e

if [ $EUID -gt 0 ]; then
    echo "Please run as root"
    exit 1
fi

help() {
    echo "install-ee username password control-tower-address"
    exit 1
}

install_docker() {
    echo "Installing docker"
    apt-get update
    apt-get -y install jq

    mkdir -p /etc/docker
    default_if=$(ip route | awk '$1=="default"{print $5}')
    insec_reg=$(ip route | awk '$3=="'"$default_if"'"{print $1}' | jq -R -s -c 'split("\n")|map(select(. != ""))')
    cat <<EOF > /etc/docker/daemon.json
{
   "iptables": false,
   "userns-remap": "default",
   "bridge": "none",
   "insecure-registries": $insec_reg
}
EOF

    apt-get -y install \
        ca-certificates \
        curl \
        gnupg \
        lsb-release
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
    echo \
        "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
        $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null
    apt-get update
    apt-get install -y docker-ce docker-ce-cli containerd.io

}

username="$1"
password="$2"
ct_address="$3"

if [ -z "$username" ]; then
    echo
    echo "Missing username"
    help
fi

if [ -z "$password" ]; then
    echo
    echo "Missing password"
    help
fi

if [ -z "$ct_address" ]; then
    echo
    echo "Missing control tower address"
    help
fi

docker > /dev/null 2>&1 || install_docker


api="api.$ct_address"

echo "Downloading edge enforcer"
echo "$password" | docker login --username "$username" --password-stdin "registry.$ct_address"

docker pull "registry.$ct_address/supd:new"
docker tag "registry.$ct_address/supd:new" "avassa/supd:current"

echo "Downloading supctl"
curl -so /usr/local/bin/supctl "https://$api/supctl"
chmod +x /usr/local/bin/supctl

## Disable iptables
sudo iptables -F
sudo iptables -X
iptables -F -t nat
iptables -P FORWARD ACCEPT

## Fix state dir
mkdir -p /var/lib/supd/state
mkdir -p /etc/supd

jq -h > /dev/null 2>&1 || apt-get install -y jq

echo "Get site config"
echo "$password" | supctl --host "$api" 'do' login "$username"
# supctl 'do' system sites "$site_name" get-initial-site-config > /etc/supd/supd.conf

[ -f /etc/supd/supd.conf ] || cat <<EOF > /etc/supd/supd.conf
host-id: DMI:product_uuid
initial-site-config:
  call-home-servers:
    - $api
EOF

curl -O "https://$api/scripts/start-supd"
chmod +x start-supd
mv start-supd /usr/sbin/.

curl -O "https://$api/scripts/supd.service"
mv supd.service /etc/systemd/system/.

ufw disable
systemctl enable supd
systemctl start supd

echo "To see the host-id, please run:"
echo "sudo docker exec supd /supd/bin/supd hostid"
