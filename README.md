# Install Scripts

The scripts assumes a user in Control Tower with the `pull-supd` policy attached. 

Such user can be created in the UI, e.g.

![sw-user](docs/sw-user.png)

The scripts can be run as this
```shell
curl -sOf https://gitlab.com/avassa-public/install-scripts/-/raw/main/install-ee-debian11
chmod +x install-ee-debian11
sudo ./install-ee-debian11 sw@avassa.io <password> my-env.my-org.avassa.net
```
